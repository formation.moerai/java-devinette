import java.io.*;

import java.io.IOException;


class Devinette {
    public static void consigne(int nbreTentative){
                System.out.println("******************************************");
                System.out.println("JEU DE DEVINETTE");
                System.out.println("L'ORDINATEUR VA CHOISIR UN NOMBRE ENTRE 0 ET 100");
                System.out.println("TU AS "+ nbreTentative +" TENTATIVES POUR TROUVER CE NOMBRE");
                 System.out.println("******************************************");
    }


    public static void main(String[] args) {
        //VARIABLE
         Console consolVar = System.console(); 
         int minimum=0;
         int maximum=100;
        
        int nbreRandom=(int)(((Math.random()*(maximum - minimum))) + minimum);
        int choixUtilisateur=0;
        int nbreDeCoupPossible= 5;
        int nbreDeCoupJouer= 0;
        boolean trouver=false;

        consigne(nbreDeCoupPossible);

        //DEBUT DE LA BOUCLE DE CONTROLE DU JEUX
        for(int i=0;i<nbreDeCoupPossible;i++){
            System.out.println("");
            //   c. récupérer le choix de l’utilisateur
            choixUtilisateur= Integer.parseInt(consolVar.readLine( "Entrez un nombre : ")) ; 
            nbreDeCoupJouer++;
        
            //CONTROLE
            if(choixUtilisateur<nbreRandom){
                System.out.print("le nombre a trouver est plus grand que "+choixUtilisateur);   
            }
            else if(choixUtilisateur>nbreRandom){
            System.out.print("le nombre a trouver est plus petit que "+choixUtilisateur);   
            }
            else if(choixUtilisateur==nbreRandom){ 
                System.out.println("******************************************");
                 System.out.println("******************************************");
                System.out.println("Tu as gagner en "+(nbreDeCoupJouer)+" coups");
                 System.out.println("******************************************");
                 System.out.println("******************************************");
                trouver=true;
                break;
            }
            
        }
        //AFFICHAGE SI MAUVAISE REPONSE
       if (nbreDeCoupJouer==nbreDeCoupPossible){
           System.out.println("");
           System.out.println("******************************************");
           System.out.println("******************************************");
           System.out.println("Desole tu as perdu");
           System.out.println("le nombre a trouver etait :"+nbreRandom);
           System.out.println("******************************************");
           System.out.println("******************************************");
       }
    
    }
}
